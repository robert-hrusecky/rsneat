pub mod matrix;

pub use self::matrix::Matrix;

use rand;

pub fn sigmoid(x: f64, slope: f64) -> f64 {
	1.0 / (1.0 + (-slope * x).exp())
}

pub fn rand_weight() -> f64 {
	rand::random::<f64>() * 2.0 - 1.0
}
