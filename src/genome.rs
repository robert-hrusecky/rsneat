use crate::util;
use crate::Neat;
use rand::seq::SliceRandom;
use rand::Rng;

use std::cmp;
use std::error::Error;
use std::io;
use std::io::prelude::*;
use std::io::{BufReader, BufWriter};

type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Clone, Debug)]
struct Gene {
    innovation: usize,
    input: usize,
    output: usize,
    weight: f64,
    enabled: bool,
}

/// The blueprint for a neural network output by the neat algorithm
#[derive(Clone, Debug)]
pub struct Genome {
    genes: Vec<Gene>,
    neuron_count: usize,
    input_count: usize,
    output_count: usize,
    pub fitness: f64,
}

impl Genome {

    /// Create an empty genome with the specified number of inputs and outputs.
    pub fn new(input_count: usize, output_count: usize) -> Self {
        Self {
            genes: Vec::new(),
            neuron_count: input_count + output_count,
            input_count,
            output_count,
            fitness: 0.0,
        }
    }

    /// Read and de-serialize bytes into a new genome from input source `reader`.
    pub fn read_from(reader: &mut impl Read) -> Result<Self> {
        let mut reader = BufReader::new(reader);
        let mut result = Self {
            genes: Vec::new(),
            neuron_count: 0,
            input_count: 0,
            output_count: 0,
            fitness: 0.0,
        };
        let mut line = String::new();

        reader.read_line(&mut line)?;
        result.neuron_count = line.trim().parse()?;
        line.clear();
        reader.read_line(&mut line)?;
        result.input_count = line.trim().parse()?;
        line.clear();
        reader.read_line(&mut line)?;
        result.output_count = line.trim().parse()?;
        line.clear();
        reader.read_line(&mut line)?;
        result.fitness = line.trim().parse()?;
        line.clear();

        reader.read_line(&mut line)?;
        let len: usize = line.trim().parse()?;
        line.clear();
        result.genes.reserve_exact(len);

        for _ in 0..len {
            let mut gene = Gene {
                innovation: 0,
                input: 0,
                output: 0,
                weight: 0.0,
                enabled: false,
            };

            reader.read_line(&mut line)?;
            gene.innovation = line.trim().parse()?;
            line.clear();
            reader.read_line(&mut line)?;
            gene.input = line.trim().parse()?;
            line.clear();
            reader.read_line(&mut line)?;
            gene.output = line.trim().parse()?;
            line.clear();
            reader.read_line(&mut line)?;
            gene.weight = line.trim().parse()?;
            line.clear();
            reader.read_line(&mut line)?;
            gene.enabled = line.trim().parse()?;
            line.clear();

            result.genes.push(gene);
        }

        Ok(result)
    }

    /// Serialize this genome to the given output source
    pub fn write(&self, writer: &mut impl Write) -> io::Result<()> {
        let mut writer = BufWriter::new(writer);
        writeln!(&mut writer, "{}", self.neuron_count)?;
        writeln!(&mut writer, "{}", self.input_count)?;
        writeln!(&mut writer, "{}", self.output_count)?;
        writeln!(&mut writer, "{}", self.fitness)?;
        writeln!(&mut writer, "{}", self.genes.len())?;
        for gene in &self.genes {
            writeln!(&mut writer, "{}", gene.innovation)?;
            writeln!(&mut writer, "{}", gene.input)?;
            writeln!(&mut writer, "{}", gene.output)?;
            writeln!(&mut writer, "{}", gene.weight)?;
            writeln!(&mut writer, "{}", gene.enabled)?;
        }
        Ok(())
    }

    fn delta(mom: &Self, dad: &Self, neat: &Neat) -> f64 {
        let mut disjoint_count = 0;
        let excess_count;
        let mut weight_average = 0.0;
        let mut match_count = 0;
        let n = cmp::max(mom.genes.len(), dad.genes.len());

        let mut mom_it = mom.genes.iter().peekable();
        let mut dad_it = dad.genes.iter().peekable();
        // while both parents have genes left

        loop {
            let (mom_gene, dad_gene);
            match mom_it.peek() {
                Some(g) => mom_gene = g,
                None => break,
            }
            match dad_it.peek() {
                Some(g) => dad_gene = g,
                None => break,
            }

            if mom_gene.innovation < dad_gene.innovation {
                mom_it.next();
                disjoint_count += 1;
            // mom has disjoint gene
            } else if mom_gene.innovation > dad_gene.innovation {
                dad_it.next();
                disjoint_count += 1;
            // dad has disjoint gene
            } else {
                let mom_gene = mom_it.next().unwrap();
                let dad_gene = dad_it.next().unwrap();
                // matching gene
                weight_average += (mom_gene.weight - dad_gene.weight).abs();
                match_count += 1;
            }
        }
        weight_average /= match_count as f64;

        // handle excess genes
        excess_count = mom_it.count() + dad_it.count();

        let delta = (neat.delta_c1() * excess_count as f64
            + neat.delta_c2() * disjoint_count as f64)
            / n as f64
            + neat.delta_c3() * weight_average;
        // println!("n: {} excess: {} disjoint: {} delta: {}", n, excess_count, disjoint_count, delta);
        delta
    }

    pub fn are_compatible(mom: &Self, dad: &Self, neat: &Neat) -> bool {
        Self::delta(mom, dad, neat) < neat.delta_threshold()
    }

    pub fn crossover(mom: &Self, dad: &Self, neat: &Neat) -> Self {
        debug_assert!(mom.input_count == dad.input_count && mom.output_count == dad.output_count);
        // mom always has larger or equal fitness.
        let (mom, dad) = if mom.fitness > dad.fitness {
            (mom, dad)
        } else if mom.fitness < dad.fitness {
            (dad, mom)
        } else {
            // in the case of equal fitness we choose the smaller genome
            if mom.genes.len() < dad.genes.len() {
                (mom, dad)
            } else {
                (dad, mom)
            }
        };

        let mut mom_it = mom.genes.iter().peekable();
        let mut dad_it = dad.genes.iter().peekable();
        let mut child_genes: Vec<Gene> = Vec::new();

        let mut max_neuron = mom.input_count + mom.output_count;

        // while both parents have genes left
        loop {
            let (mom_gene, dad_gene);
            match mom_it.peek() {
                Some(g) => mom_gene = g,
                None => break,
            }
            match dad_it.peek() {
                Some(g) => dad_gene = g,
                None => break,
            }
            let enabled;
            let new_gene = if mom_gene.innovation < dad_gene.innovation {
                let mom_gene = mom_it.next().unwrap();
                // mom has disjoint gene
                // inherit
                enabled = mom_gene.enabled;
                mom_gene
            } else if mom_gene.innovation > dad_gene.innovation {
                dad_it.next();
                // dad has disjoint gene
                // do not inherit
                continue;
            } else {
                let mom_gene = mom_it.next().unwrap();
                let dad_gene = dad_it.next().unwrap();
                // same innovations, pick from random parent
                enabled = mom_gene.enabled || rand::random::<f64>() > neat.disable_chance();
                // enabled = mom_gene.enabled;
                if rand::random::<f64>() < 0.5 {
                    mom_gene
                } else {
                    dad_gene
                }
            };
            max_neuron = cmp::max(cmp::max(new_gene.input, new_gene.output), max_neuron);
            // add the new gene to the child genome, with disabled parent genes having a
            // chance of staying disabled.
            child_genes.push(Gene {
                enabled,
                ..*new_gene
            });
        }

        // handle excess genes
        // dad cannot have excess genes that we want because he is always worse
        for new_gene in mom_it {
            max_neuron = cmp::max(cmp::max(new_gene.input, new_gene.output), max_neuron);
            child_genes.push(Gene { ..*new_gene });
        }

        Self {
            genes: child_genes,
            fitness: 0.0,
            neuron_count: max_neuron + 1,
            ..*mom
        }
    }

    pub fn add_neurons(&mut self, count: usize) {
        self.neuron_count += count;
    }

    fn is_input(&self, input: usize) -> bool {
        input < self.input_count
    }

    fn is_output(&self, output: usize) -> bool {
        output >= self.input_count && output < self.input_count + self.output_count
    }

    fn valid_connection(&self, input: usize, output: usize) -> bool {
        !(self.is_input(output) || self.is_output(input))
    }

    // Add a new connection to the genome in the resulting neural network.
    pub fn add_connection(&mut self, input: usize, output: usize, weight: f64, neat: &mut Neat) {
        debug_assert!(self.valid_connection(input, output));
        let innovation = neat.innovation(input, output);
        match self
            .genes
            .binary_search_by_key(&innovation, |g| g.innovation)
        {
            Ok(_) => panic!("Connection weight already exists"),
            Err(i) => self.genes.insert(
                i,
                Gene {
                    innovation,
                    input,
                    output,
                    weight,
                    enabled: true,
                },
            ),
        }
    }

    /// Add a connection between two randomly selected network nodes
    pub fn add_random_connection(&mut self, neat: &mut Neat) {
        let mut rng = rand::thread_rng();
        for _ in 0..20 {
            let rand: usize = rng.gen_range(0, self.neuron_count - self.output_count);
            let input = if rand >= self.input_count {
                rand + self.output_count
            } else {
                rand
            };
            let output: usize = rng.gen_range(self.input_count, self.neuron_count);

            let gene = &mut self
                .genes
                .iter_mut()
                .find(|x| x.input == input && x.output == output);
            match gene {
                Some(gene) if gene.enabled => (),
                Some(gene) => {
                    gene.enabled = true;
                    gene.weight = util::rand_weight();
                    break;
                }
                None => {
                    self.add_connection(input, output, util::rand_weight(), neat);
                    break;
                }
            }
        }
    }

    /// Add a random neuron to the network.
    fn add_random_neuron(&mut self, neat: &mut Neat) {
        let mut rng = rand::thread_rng();
        let (input, new, output, weight);
        {
            let mut enabled_genes: Vec<_> = self.genes.iter_mut().filter(|x| x.enabled).collect();
            let src_gene = enabled_genes.choose_mut(&mut rng);
            if src_gene.is_none() {
                return;
            }
            let src_gene = src_gene.unwrap();
            src_gene.enabled = false;
            input = src_gene.input;
            new = self.neuron_count;
            self.neuron_count += 1;
            output = src_gene.output;
            weight = src_gene.weight;
        }

        self.add_connection(input, new, 1.0, neat);
        self.add_connection(new, output, weight, neat);
    }

    fn mutate_toggle_enable(&mut self) {
        let mut rng = rand::thread_rng();
        if let Some(gene) = self.genes.choose_mut(&mut rng) {
            let innovation = gene.innovation;
            let input = gene.input;
            if !gene.enabled {
                gene.enabled = true;
            } else if let Some(gene) = self
                .genes
                .iter_mut()
                .find(|g| g.innovation != innovation && g.input == input)
            {
                gene.enabled = false;
            }
        }
    }

    pub fn mutate_weights(&mut self, neat: &Neat) {
        for gene in &mut self.genes {
            if rand::random::<f64>() < neat.weight_reset_chance() {
                gene.weight = util::rand_weight();
            } else {
                gene.weight += util::rand_weight();
            }
        }
    }

    pub fn attempt_mutate(&mut self, neat: &mut Neat) {
        // perturb weights
        if rand::random::<f64>() < neat.weight_mutation_chance() {
            self.mutate_weights(neat);
        }
        // add connection
        if rand::random::<f64>() < neat.new_weight_chance() {
            self.add_random_connection(neat);
        }
        // add node
        if rand::random::<f64>() < neat.new_neuron_chance() {
            self.add_random_neuron(neat);
        }

        if rand::random::<f64>() < neat.new_weight_chance() {
            self.mutate_toggle_enable();
        }
    }

    pub fn input_count(&self) -> usize {
        self.input_count
    }

    pub fn output_count(&self) -> usize {
        self.output_count
    }

    pub fn hidden_count(&self) -> usize {
        self.neuron_count - self.input_count - self.output_count
    }

    pub fn connections<'a>(&'a self) -> impl Iterator<Item = (usize, usize, f64)> + 'a {
        self.genes
            .iter()
            .filter(|g| g.enabled)
            .map(|g| (g.input, g.output, g.weight))
    }
}

#[cfg(test)]
mod tests {
    pub use super::*;
    #[test]
    fn genome_simple() {
        let mut neat = Neat::new();
        let mut mom = Genome::new(3, 1);
        mom.add_neurons(1);
        mom.add_connection(0, 3, 1.0, &mut neat);
        mom.add_connection(1, 3, 1.0, &mut neat);
        mom.add_connection(2, 3, 1.0, &mut neat);
        mom.add_connection(1, 4, 1.0, &mut neat);
        mom.add_connection(4, 3, 1.0, &mut neat);
        mom.add_connection(0, 4, 1.0, &mut neat);
        let mut dad = Genome::new(3, 1);
        dad.add_neurons(2);
        dad.add_connection(0, 3, 0.0, &mut neat);
        dad.add_connection(1, 3, 0.0, &mut neat);
        dad.add_connection(2, 3, 0.0, &mut neat);
        dad.add_connection(1, 4, 0.0, &mut neat);
        dad.add_connection(4, 3, 0.0, &mut neat);
        dad.add_connection(4, 5, 0.0, &mut neat);
        dad.add_connection(5, 3, 0.0, &mut neat);
        dad.add_connection(2, 4, 0.0, &mut neat);
        dad.add_connection(0, 5, 0.0, &mut neat);

        let mut child = Genome::crossover(&mom, &dad, &mut neat);
        println!("{:#?}", child);
        neat.params.new_weight_chance = 0.0;
        neat.params.new_neuron_chance = 1.0;
        child.attempt_mutate(&mut neat);
        println!("Mutated:");
        println!("{:#?}", child);
    }

    #[test]
    fn are_compatible_test() {
        let mut neat = Neat::new();
        let mut mom = Genome::new(3, 1);
        mom.add_neurons(1);
        mom.add_connection(0, 3, 1.0, &mut neat);
        mom.add_connection(1, 3, 1.0, &mut neat);
        mom.add_connection(2, 3, 1.0, &mut neat);
        mom.add_connection(1, 4, 1.0, &mut neat);
        mom.add_connection(4, 3, 1.0, &mut neat);
        mom.add_connection(0, 4, 1.0, &mut neat);

        let mut dad = Genome::new(3, 1);
        dad.add_neurons(1);
        dad.add_connection(0, 3, 1.0, &mut neat);
        dad.add_connection(1, 3, 1.0, &mut neat);
        dad.add_connection(2, 3, 1.0, &mut neat);
        dad.add_connection(1, 4, 1.0, &mut neat);
        dad.add_connection(4, 3, 1.0, &mut neat);
        dad.add_connection(0, 4, 1.0, &mut neat);

        assert!(Genome::delta(&mom, &dad, &neat) == 0.0);

        let mut count = 0;
        let limit = 100;
        while Genome::are_compatible(&mom, &dad, &neat) && count < limit {
            dad.attempt_mutate(&mut neat);
            count += 1;
        }
        println!("Mutations: {}", count);
        assert!(count < limit);
    }
}
