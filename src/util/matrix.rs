use std::ops::{Index, IndexMut};

pub struct Matrix {
	vals: Box<[f64]>,
	rows: usize,
	cols: usize,
}

impl Matrix {
	pub fn new(rows: usize, cols: usize) -> Matrix {
		Matrix {
			vals: vec![0f64; rows * cols].into_boxed_slice(),
			rows,
			cols,
		}
	}

	#[allow(dead_code)]
	pub fn rows(&self) -> usize {
		self.rows
	}

	#[allow(dead_code)]
	pub fn cols(&self) -> usize {
		self.cols
	}
}

impl Index<usize> for Matrix {
	type Output = [f64];

	fn index(&self, row: usize) -> &[f64] {
		let pos = row * self.cols;
		&self.vals[pos..pos + self.cols]
	}
}

impl IndexMut<usize> for Matrix {
	fn index_mut(&mut self, row: usize) -> &mut [f64] {
		let pos = row * self.cols;
		&mut self.vals[pos..pos + self.cols]
	}
}

#[cfg(test)]
mod tests {
	pub use super::*;

	#[test]
	fn matrix_zero() {
		let mat = Matrix::new(3, 3);
		for i in 0..mat.rows() {
			for j in 0..mat.cols() {
				assert_eq!(mat[i][j], 0.0);
			}
		}
	}

	#[test]
	fn matrix_identity() {
		let mut mat = Matrix::new(3, 3);
		for i in 0..mat.rows() {
			mat[i][i] = 1.0;
		}
	}

	#[should_panic]
	#[test]
	fn matrix_bounds1() {
		let mat = Matrix::new(3, 3);
		println!("Out of bounds: {}", mat[7][0]);
	}

	#[should_panic]
	#[test]
	fn matrix_bounds2() {
		let mat = Matrix::new(3, 3);
		println!("Out of bounds: {}", mat[0][7]);
	}

	#[should_panic]
	#[test]
	fn matrix_bounds3() {
		let mut mat = Matrix::new(3, 3);
		mat[7][0] = 1.0;
	}

	#[should_panic]
	#[test]
	fn matrix_bounds4() {
		let mut mat = Matrix::new(3, 3);
		mat[0][7] = 1.0;
	}
}
