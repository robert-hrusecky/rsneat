use rayon::prelude::*;
use rayon::{ThreadPool, ThreadPoolBuilder};

use crate::genome::Genome;
use crate::network::Network;
use crate::Neat;
use rand::seq::SliceRandom;
use rand::Rng;
use std::iter;
use std::mem;

struct Species {
	pub members: Vec<Genome>,
	pub children: Vec<Genome>,
	pub max_fitness: f64,
	pub total_fitness: f64,
	pub age: usize,
	pub expected_offspring: usize,
}

impl Species {
	fn new(rep: Genome) -> Self {
		let mut children = Vec::new();
		children.push(rep);
		Species {
			members: Vec::new(),
			children,
			max_fitness: 0.0,
			total_fitness: 0.0,
			age: 0,
			expected_offspring: 0,
		}
	}

	fn swap(&mut self) {
		mem::swap(&mut self.members, &mut self.children);
		self.children.clear();
	}

	fn champ(&self) -> &Genome {
		if self.members.len() > 0 {
			&self.members[0]
		} else {
			&self.children[0]
		}
	}

	fn compatible_with(&self, genome: &Genome, neat: &Neat) -> bool {
		Genome::are_compatible(self.champ(), genome, neat)
	}
}

/// Stores the population of genomes for the simulation.
pub struct Population {
	species: Vec<Species>,
	gen: usize,
	champs: Vec<Genome>,
	pool: ThreadPool,
}

impl Population {
    
    /// Create a new population of genomes with the given network layout
    pub fn new(input_count: usize, output_count: usize, neat: &mut Neat) -> Self {
		let mut species = Vec::new();
		let mut founder = Genome::new(input_count, output_count);
		for _ in 0..input_count / 2 {
			founder.add_random_connection(neat);
		}
		let mut champ = founder.clone();
		champ.fitness = 0.0;
		species.push(Species::new(founder));

		let mut result = Population {
			species,
			gen: 0,
			champs: Vec::new(),
			pool: ThreadPoolBuilder::new().build().unwrap(),
		};
		result.champs.push(champ);

		for _ in 1..neat.population_size() {
			let mut new = Genome::new(input_count, output_count);
			for _ in 0..input_count / 2 {
				new.add_random_connection(neat);
			}
			result.assign_to_species(new, neat);
		}

		for s in &mut result.species {
			s.swap();
		}

		result
	}

    /// Create a population cloned and mutated from the given genome
	pub fn clone_from(founder: Genome, neat: &Neat) -> Self {
		let mut species = Vec::new();
		let mut champ = founder.clone();
		champ.fitness = 0.0;
		species.push(Species::new(founder));

		let mut result = Population {
			species,
			gen: 0,
			champs: Vec::new(),
			pool: ThreadPoolBuilder::new().build().unwrap(),
		};
		result.champs.push(champ);

		for _ in 1..neat.population_size() {
			let mut clone = result.species[0].champ().clone();
			clone.mutate_weights(neat);
			result.assign_to_species(clone, neat);
		}

		for s in &mut result.species {
			s.swap();
		}

		result
	}

    /// reference to the best genome so far
	pub fn champ(&self) -> &Genome {
		&self.champs[0]
	}

    /// reference to the genomes in the best species so far
	pub fn champs(&self) -> &Vec<Genome> {
		&self.champs
	}

	fn assign_to_species(&mut self, child: Genome, neat: &Neat) {
		let mut species_it = self.species.iter_mut();
		loop {
			match species_it.next() {
				Some(curr_species) => {
					if curr_species.compatible_with(&child, neat) {
						curr_species.children.push(child);
						break;
					}
				}
				None => {
					self.species.push(Species::new(child));
					break;
				}
			}
		}
	}

    /// Generate an iterator that can be used to evaluate each network in the population.
    /// A fitness can be assigned to each network through the returned references.
	pub fn iter_fitness<'a>(&'a mut self) -> Box<dyn Iterator<Item = (Network, &'a mut f64)> + 'a> {
		let mut result: Box<dyn Iterator<Item = (Network, &mut f64)>> = Box::new(iter::empty());
		for s in &mut self.species {
			result = Box::new(
				result.chain(
					s.members
						.iter_mut()
						.map(|g| (Network::from_genome(g), &mut g.fitness)),
				),
			);
		}
		result
	}

    /// Run a generation using the internal threadpool with the given evaluation function
	pub fn quick_generation<F>(&mut self, evaluator: F, trials: usize, neat: &mut Neat) -> f64
	where
		F: Fn(Network) -> f64 + Send + Sync,
	{
		// look into paralellizing this
		// for s in &mut self.species {
		// 	for g in &mut s.members {
		// 		g.fitness = evaluator(Network::from_genome(g));
		// 	}
		// }
		let species = &mut self.species;
		self.pool.install(|| {
			species.par_iter_mut().for_each(|s| {
				s.members.par_iter_mut().for_each(|g| {
					g.fitness = 0.0;
					for _ in 0..trials {
						g.fitness += evaluator(Network::from_genome(g))
					}
					g.fitness /= trials as f64;
				});
			})
		});

		self.evaluate_generation(neat);
		return self.champ().fitness;
	}

	fn compute_expected_offspring(&mut self, neat: &Neat) {
		let mut pop_total_fitness = 0.0;
		for s in &mut self.species {
			let new_total_fitness = s.members.iter().map(|g| g.fitness).sum();

			if new_total_fitness <= s.total_fitness {
				s.age += 1;
			} else {
				s.age = 0;
			}

			s.total_fitness = new_total_fitness;
			pop_total_fitness += s.total_fitness;
		}

		let mut total_expected_offspring = 0;
		if pop_total_fitness > 0.0 {
			for s in &mut self.species {
				if s.age < neat.species_death_time() {
					s.expected_offspring = 0;
				} else {
					s.expected_offspring = (neat.population_size() as f64 * s.total_fitness
						/ pop_total_fitness)
						.floor() as usize;
				}
				total_expected_offspring += s.expected_offspring;
			}
		}

		if total_expected_offspring == 0 {
			let expected_offspring = neat.population_size() / self.species.len();
			for s in &mut self.species {
				s.expected_offspring = expected_offspring;
				total_expected_offspring += s.expected_offspring;
			}
		}

		// make up for lost percision by distributing the remaining offspring across species
		let mut i = 0;
		while total_expected_offspring < neat.population_size() {
			self.species[i].expected_offspring += 1;
			total_expected_offspring += 1;
			i += 1;
			i %= self.species.len();
		}

		// for (i, s) in self.species.iter().enumerate() {
		// 	println!("Species {} expects {} offspring.", i, s.expected_offspring);
		// }
	}

	fn reproduce(&mut self, neat: &mut Neat) {
		let initial_species_limit = self.species.len();
		for i in 0..initial_species_limit {
			if self.species[i].expected_offspring > neat.champion_copy_size() {
				self.assign_to_species(self.species[i].champ().clone(), neat);
				self.species[i].expected_offspring -= 1;
			}

			let rng = &mut rand::thread_rng();
			while self.species[i].expected_offspring > 0 {
				let mut child = if rand::random::<f64>() < neat.no_crossover_chance() {
					self.species[i].members.choose(rng).unwrap().clone()
				} else {
					let mom = self.species[i].members.choose(rng).unwrap();
					let dad_species = if rand::random::<f64>() < neat.interspecies_mating_rate() {
						&self.species[rng.gen_range(0, initial_species_limit)]
					} else {
						&self.species[i]
					};
					let dad = dad_species.members.choose(rng).unwrap();
					Genome::crossover(mom, dad, neat)
				};
				child.attempt_mutate(neat);
				self.assign_to_species(child, neat);
				self.species[i].expected_offspring -= 1;
			}
		}
	}

    /// Evaluate the performance of a generation. Use after calling `iter_fitness`
	pub fn evaluate_generation(&mut self, neat: &mut Neat) {
		// println!("Species: {}", self.species.len());

		assert!(self.species.len() > 0);

		// adjust the fitness of members for fitness sharing
		self.champs.clear();
		for s in &mut self.species {
			debug_assert!(s.members.len() > 0);

			// find the champ
			self.champs.push(
				s.members
					.iter()
					.max_by(|a, b| a.fitness.partial_cmp(&b.fitness).unwrap())
					.unwrap()
					.clone(),
			);

			let size = s.members.len() as f64;
			for g in &mut s.members {
				assert!(g.fitness >= 0.0);
				g.fitness /= size;
			}
			s.members
				.sort_unstable_by(|a, b| a.fitness.partial_cmp(&b.fitness).unwrap().reverse());
		}
		self.champs
			.sort_unstable_by(|a, b| a.fitness.partial_cmp(&b.fitness).unwrap().reverse());

		self.compute_expected_offspring(neat);

		// kill off members with low fitness
		let survival_size =
			(neat.survival_threshold() * neat.population_size() as f64).floor() as usize + 1;
		// println!("Survival size: {}", survival_size);
		for s in &mut self.species {
			s.members.truncate(survival_size);
			// println!("Species mating with {} members.", s.members.len());
		}

		self.reproduce(neat);

		for s in &mut self.species {
			s.swap();
		}

		// remove empty species
		self.species.retain(|s| s.members.len() > 0);

		self.gen += 1;
	}

    /// The current generation number
	pub fn gen(&self) -> usize {
		self.gen
	}

    /// The current number of species
	pub fn species_count(&self) -> usize {
		self.species.len()
	}
}
