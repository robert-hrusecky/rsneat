# rsneat

rsneat is an open source crate that implements the NEAT genetic algorithm. The implementation
is based off of the original [2002 paper][paper].

[paper]: http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf

The purpose of this project was to help me learn Rust and the Rust ecosystem. This project
should probably not be used for a serious project without extreme caution.

# Example

This example uses the neat simulation to find an algorithm that computes the xor function
```rust

let mut neat = Neat::new();
let mut founder = Genome::new(3, 1);
founder.add_connection(0, 3, 0.0, &mut neat);
founder.add_connection(1, 3, 0.0, &mut neat);
founder.add_connection(2, 3, 0.0, &mut neat);

let mut pop = Population::clone_from(founder, &neat);
loop {
    let mut correct = true;
    for (mut n, fitness) in pop.iter_fitness() {
        correct = true;
        let data = [
            (vec![0.0, 0.0, 1.0], 0.0, false),
            (vec![0.0, 1.0, 1.0], 1.0, true),
            (vec![1.0, 0.0, 1.0], 1.0, true),
            (vec![1.0, 1.0, 1.0], 0.0, false),
        ];

        let mut error = 0.0;
        *fitness = 0.0;
        for (input, output, c) in data.iter() {
            for _ in 0..5 {
                let _ = n.activate(input.clone().into_iter());
            }
            let result = n.activate(input.clone().into_iter()).next().unwrap();
            let result_error = (result - output).abs();
            error += result_error;

            let result = result >= 0.5;
            if *c != result {
                correct = false;
            }

            n.reset();
        }
        *fitness += 4.0 - error;
        *fitness *= *fitness;
        if correct {
            break;
        }
    }
    if correct {
        pop.champ().write(&mut std::fs::File::create("champ.neat").unwrap()).unwrap();
        println!("Got a champ after {} gens!", pop.gen());
    }
    pop.evaluate_generation(&mut neat);
}
```
